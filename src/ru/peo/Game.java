package ru.peo;

import java.io.IOException;

/**
 * Created by Elena on 29.09.2017.
 */

//Заканчивает игру. Выводит количество попыток
public class Game {
    private static int tries = 0;
    public static void main(String[] args) throws IOException {
        game();
        go();
        finish();
    }
    private static void go() {
        Ship ship = new Ship();
        int beginPoint = (int) (Math.random() * 8);
        ship.setLocation(beginPoint);
        System.out.println(ship.toString());
    }
    private static int game() throws IOException {
        String result;
        System.out.println("Введите координату нахождения корабля: ");
        do {
            tries++;
            result = Ship.shoots(Additional.input());
            System.out.println(result);
        } while (!result.equals("Потоплен!"));
        return tries;
    }
    private static void finish() {
        System.out.println("Кол-во ходов: " + tries);
    }
}
