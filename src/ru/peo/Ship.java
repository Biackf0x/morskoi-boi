package ru.peo;

import java.util.ArrayList;

/**
 * Created by Elena on 07.10.2017.
 */
public class Ship {
    private static ArrayList<String> location = new ArrayList<>();
    public void setLocation(int beginPoint) {
        location.add(Integer.toString(beginPoint));
        location.add(Integer.toString(beginPoint + 1));
        location.add(Integer.toString(beginPoint + 2));
    }
    public ArrayList<String> getLocation() {
        return location;
    }
    public static String shoots(String shot) {
        int index = location.indexOf(shot);
        String result = "Мимо";
        if (index != -1) {
            location.remove(index);
            if (location.isEmpty()) {
                result = "Потоплен!";
            } else {
                result = "Ранен!";
            }
        } return result;
    }
    @Override
    public String toString() {
        return "Корабль {" +
                "расположение = " + location +
                '}';
    }
}